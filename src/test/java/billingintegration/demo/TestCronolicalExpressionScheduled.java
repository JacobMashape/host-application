package billingintegration.demo;

import org.junit.jupiter.api.Test;
import org.springframework.scheduling.support.CronSequenceGenerator;

import java.text.SimpleDateFormat;
import java.util.Calendar;

//@RunWith(SpringJUnit4ClassRunner.class)
//@SpringBootTest
public class TestCronolicalExpressionScheduled {

    @Test
    public void testCronExpression(){
        CronSequenceGenerator cronSequenceGenerator = new CronSequenceGenerator("0 0 0 1-3 * 1-5");
        Calendar cal = Calendar.getInstance();
        cal.set(2019,0,
                4);
//        cal.add(Calendar.DATE, 2); // add two days to current date
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy MMM dd HH:mm:ss");
        System.out.println("current date "+sdf.format(cal.getTime()));
        System.out.println("Next cron trigger date cron seqquence "+cronSequenceGenerator.next(cal.getTime()));

    }
}
