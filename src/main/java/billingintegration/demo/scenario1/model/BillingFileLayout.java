package billingintegration.demo.scenario1.model;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Objects;

@Entity
public class BillingFileLayout {

    public enum SubService {
        ZAROUT,
        CCYOUT
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "Id", updatable = false, nullable = false)
    private Long id;

    @Column(name = "ServiceName", length = 18, nullable = false)
    private static final String ServiceName = "INTEGRATEDSERVICES";

    @Column(name = "ClientSwiftAddress", length = 11, nullable = false)
    private String clientSwiftAddress;

    @Column(name = "SubService", length = 6, nullable = false)
    @Enumerated(EnumType.STRING)
    private SubService subService;

    @Column(name = "Date", length = 8, nullable = false)
    @DateTimeFormat(pattern = "YYYYMMDD")
    private LocalDate date;

    @Column(name = "UsageStats", length = 6, nullable = false)
    private long usageStats;

    public BillingFileLayout(String clientSwiftAddress,long usageStats){
        this.clientSwiftAddress=clientSwiftAddress;
        this.usageStats =usageStats;
    }

    public BillingFileLayout() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getClientSwiftAddress() {
        return clientSwiftAddress;
    }

    public void setClientSwiftAddress(String clientSwiftAddress) {
        this.clientSwiftAddress = clientSwiftAddress;
    }

    public SubService getSubService() {
        return subService;
    }

    public void setSubService(SubService subService) {
        this.subService = subService;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public long getUsageStats() {
        return usageStats;
    }

    public void setUsageStats(long usageStats) {
        this.usageStats = usageStats;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BillingFileLayout)) return false;
        BillingFileLayout that = (BillingFileLayout) o;
        return getUsageStats() == that.getUsageStats() &&
                getClientSwiftAddress().equals(that.getClientSwiftAddress()) &&
                getSubService() == that.getSubService() &&
                getDate().equals(that.getDate());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getClientSwiftAddress(), getSubService(), getDate(), getUsageStats());
    }

    @Override
    public String toString() {
        String formatRule = "%";
        String charType = "s";
        return  String.format(formatRule +(18)+ charType, ServiceName) +
                String.format(formatRule +(11)+ charType, clientSwiftAddress) +
                String.format(formatRule +(6)+ charType,subService) +
                String.format(formatRule +(8)+ charType, String.format("%tY%1$tm%1$td",date)) +
                String.format(formatRule +(6)+ charType, usageStats);
    }
}


