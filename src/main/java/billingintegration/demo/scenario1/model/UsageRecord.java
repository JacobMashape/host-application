package billingintegration.demo.scenario1.model;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
public class UsageRecord {

    public enum MessageStatus {
        Completed,
        Rejected
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "Id", updatable = false, nullable = false)
    private Long id;

    @Column(name="TransactionReference",length = 16,nullable = false)
    private String transactionReference;

    @Column(name="ClientSwiftAddress",length = 12,nullable = false)
    private String clientSwiftAddress;

    @Column(name="MessageStatus",length = 9,nullable = false)
    @Enumerated(EnumType.STRING)
    private MessageStatus messageStatus;

    @Column(name="currency",length = 3,nullable = false)
    private String currency;

    @Column(name="Amount",nullable = false)
    private BigDecimal amount;

    @Column(name="DateTimeCreated",length = 6,nullable = false)
    @DateTimeFormat(pattern = "YYYYMMDD")
    private LocalDateTime dateTimeCreated;

    public UsageRecord() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTransactionReference() {
        return transactionReference;
    }

    public void setTransactionReference(String transactionReference) {
        this.transactionReference = transactionReference;
    }

    public String getClientSwiftAddress() {
        return clientSwiftAddress;
    }

    public void setClientSwiftAddress(String clientSwiftAddress) {
        this.clientSwiftAddress = clientSwiftAddress;
    }

    public MessageStatus getMessageStatus() {
        return messageStatus;
    }

    public void setMessageStatus(MessageStatus messageStatus) {
        this.messageStatus = messageStatus;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public LocalDateTime getDateTimeCreated() {
        return dateTimeCreated;
    }

    public void setDateTimeCreated(LocalDateTime dateTimeCreated) {
        this.dateTimeCreated = dateTimeCreated;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UsageRecord)) return false;
        UsageRecord that = (UsageRecord) o;
        return getTransactionReference().equals(that.getTransactionReference()) &&
                getClientSwiftAddress().equals(that.getClientSwiftAddress()) &&
                getMessageStatus() == that.getMessageStatus() &&
                getCurrency().equals(that.getCurrency()) &&
                getAmount().equals(that.getAmount()) &&
                getDateTimeCreated().equals(that.getDateTimeCreated());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getTransactionReference(), getClientSwiftAddress(), getMessageStatus(), getCurrency(), getAmount(), getDateTimeCreated());
    }

    @Override
    public String toString() {
        return "UsageRecord{" +
                "transactionReference='" + transactionReference + '\'' +
                ", clientSwiftAddress='" + clientSwiftAddress + '\'' +
                ", messageStatus=" + messageStatus +
                ", currency='" + currency + '\'' +
                ", amount=" + amount +
                ", dateTimeCreated=" + dateTimeCreated +
                '}';
    }
}
