package billingintegration.demo.scenario1.repository;

import billingintegration.demo.scenario1.model.BillingFileLayout;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BillingFileLayoutRepository extends JpaRepository<BillingFileLayout,Long> {
}
