package billingintegration.demo.scenario1.repository;

import billingintegration.demo.scenario1.model.BillingFileLayout;
import billingintegration.demo.scenario1.model.UsageRecord;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface UsageRecordRepository extends JpaRepository<UsageRecord, Long> {

    @Modifying
    @Query(value = "insert into usage_record  (id, amount, client_swift_address, currency, date_time_created, message_status, transaction_reference)" +
            " values  (:id, :amount, :clientSwiftAddress, :currency, :dateTimeCreated, :messageStatus, :transactionReference)",
            nativeQuery = true)
    void insertUsageRecord(@Param("id") Long id, @Param("transactionReference") String transactionReference, @Param("clientSwiftAddress") String clientSwiftAddress,
                           @Param("messageStatus") String messageStatus, @Param("currency") String currency, @Param("amount") BigDecimal amount,
                           @Param("dateTimeCreated") LocalDateTime dateTimeCreated);


    @Query("SELECT " +
            "   new billingintegration.demo.scenario1.model.BillingFileLayout(v.clientSwiftAddress, count(v)) " +
            "FROM " +
            "    UsageRecord v " +
            "where " +
            "v.dateTimeCreated>=:firstDay and v.dateTimeCreated<=:lastDay and v.messageStatus=:messageStatus and v.currency=:currency" +
            "      group by " +
            "    v.clientSwiftAddress ")
    List<BillingFileLayout> findUsageRecordStatsWithZAR(@Param("firstDay") LocalDateTime firstDay, @Param("lastDay") LocalDateTime lastDay,
                                                        @Param("messageStatus") UsageRecord.MessageStatus messageStatus, @Param("currency") String currency);

    @Query("SELECT " +
            "   new billingintegration.demo.scenario1.model.BillingFileLayout(v.clientSwiftAddress, count(v)) " +
            "FROM " +
            "    UsageRecord v " +
            "where " +
            "v.dateTimeCreated>=:firstDay and v.dateTimeCreated<=:lastDay and v.messageStatus=:messageStatus and not v.currency=:currency" +
            "      group by " +
            "    v.clientSwiftAddress ")
    List<BillingFileLayout> findUsageRecordStatsWithNotZAR(@Param("firstDay") LocalDateTime firstDay, @Param("lastDay") LocalDateTime lastDay,
                                                           @Param("messageStatus") UsageRecord.MessageStatus messageStatus, @Param("currency") String currency);
}
