package billingintegration.demo.scenario1.service;

public interface UsageRecordService {

    void writeUsageStats();

}
