package billingintegration.demo.scenario1.service.impl;

import billingintegration.demo.scenario1.model.BillingFileLayout;
import billingintegration.demo.scenario1.model.UsageRecord;
import billingintegration.demo.scenario1.repository.BillingFileLayoutRepository;
import billingintegration.demo.scenario1.repository.UsageRecordRepository;
import billingintegration.demo.scenario1.service.UsageRecordService;
import billingintegration.demo.scenario1.service.util.MultipartUtility;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.List;

@Service
public class UsageRecordServiceImpl implements UsageRecordService {

    public static final String FILE_PATH = "src\\main\\resources\\extractedRecords\\" + LocalDate.now() + "_extractedRecords.txt";
    public static final String HTTP_LOCALHOST_9091_API_UPLOAD = "http://localhost:9091/api/upload";

    private final UsageRecordRepository usageRecordRepository;

    private final BillingFileLayoutRepository billingFileLayoutRepository;

    public UsageRecordServiceImpl(UsageRecordRepository usageRecordRepository, BillingFileLayoutRepository billingFileLayoutRepository) {
        this.usageRecordRepository = usageRecordRepository;
        this.billingFileLayoutRepository = billingFileLayoutRepository;
    }


    @Override
    @Transactional
    @Scheduled(cron = "${cron.expression}", zone = "Africa/Harare")
    public void writeUsageStats() {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(FILE_PATH))) {
            LocalDateTime now = LocalDateTime.now();
            Month month = now.getMonth().minus(1);
            int year = month.getValue() == 12 ? now.getYear() - 1 : now.getYear();
            boolean isLeapYear = ((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0);
            int lastDay = isLeapYear ? month.minLength() : month.maxLength();
            LocalDateTime firstDayOfLastMonth = LocalDateTime.of(year, month, 1, 0, 0);
            LocalDateTime lastDayOfLastMonth = LocalDateTime.of(year, month, lastDay, 23, 59);
            UsageRecord.MessageStatus messageStatus = UsageRecord.MessageStatus.Completed;
            List<BillingFileLayout> fileLayoutsWithZAR = this.usageRecordRepository.findUsageRecordStatsWithZAR(firstDayOfLastMonth,
                    lastDayOfLastMonth, messageStatus, "ZAR");
            List<BillingFileLayout> fileLayoutsWithNotZAR = this.usageRecordRepository.findUsageRecordStatsWithNotZAR(firstDayOfLastMonth,
                    lastDayOfLastMonth, messageStatus, "ZAR");

            fileLayoutsWithZAR.parallelStream().forEach(
                    s -> {
                        addUsageStats(writer, s, BillingFileLayout.SubService.ZAROUT);
                    }
            );
            fileLayoutsWithNotZAR.parallelStream().forEach(
                    s -> {
                        addUsageStats(writer, s, BillingFileLayout.SubService.CCYOUT);
                    }
            );

        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        }
        System.out.println("Sending file to Sample API . . .");
        sendToSampleAPI();
    }

    private void addUsageStats(BufferedWriter writer, BillingFileLayout s, BillingFileLayout.SubService subService) {
        BillingFileLayout billingFileLayout = new BillingFileLayout();
        billingFileLayout.setClientSwiftAddress(s.getClientSwiftAddress());
        billingFileLayout.setUsageStats(s.getUsageStats());
        billingFileLayout.setDate(LocalDate.now());
        billingFileLayout.setSubService(subService);
        try {
            writer.append(billingFileLayout.toString());
            writer.newLine();
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }
        this.billingFileLayoutRepository.save(billingFileLayout);
    }

    private void addDataToDatabase() {
        this.usageRecordRepository.insertUsageRecord(1L, "1AAA", "ZA-BANK",
                String.valueOf(UsageRecord.MessageStatus.Completed), "ZAR", BigDecimal.valueOf(11005.150), LocalDateTime.of(2019, 11, 15, 15, 16));
        this.usageRecordRepository.insertUsageRecord(2L, "2AAB", "EURO-BANK",
                String.valueOf(UsageRecord.MessageStatus.Rejected), "ZAR", BigDecimal.valueOf(1100155.150), LocalDateTime.of(2019, 11, 30, 20, 16));
        this.usageRecordRepository.insertUsageRecord(3L, "3ABB", "NAM-BANK",
                String.valueOf(UsageRecord.MessageStatus.Completed), "USD", BigDecimal.valueOf(311005.150), LocalDateTime.of(2019, 11, 7, 1, 16));
        this.usageRecordRepository.insertUsageRecord(4L, "4ZZZ", "USA-BANK",
                String.valueOf(UsageRecord.MessageStatus.Completed), "USD", BigDecimal.valueOf(411005.150), LocalDateTime.of(2019, 11, 19, 2, 16));
        this.usageRecordRepository.insertUsageRecord(5L, "5AZX", "USA-BANK",
                String.valueOf(UsageRecord.MessageStatus.Completed), "EUR", BigDecimal.valueOf(511005.150), LocalDateTime.of(2019, 11, 17, 15, 16));
        this.usageRecordRepository.insertUsageRecord(9L, "9ABZ", "ZA-BANK",
                String.valueOf(UsageRecord.MessageStatus.Rejected), "ZAR", BigDecimal.valueOf(611005.150), LocalDateTime.of(2019, 11, 21, 5, 16));
        this.usageRecordRepository.insertUsageRecord(10L, "10ZXW", "UK-BANK",
                String.valueOf(UsageRecord.MessageStatus.Completed), "EUR", BigDecimal.valueOf(711005.150), LocalDateTime.of(2019, 11, 26, 15, 16));
        this.usageRecordRepository.insertUsageRecord(11L, "128DWE", "UK-BANK",
                String.valueOf(UsageRecord.MessageStatus.Completed), "ZAR", BigDecimal.valueOf(811005.150), LocalDateTime.of(2019, 5, 2, 15, 16));
        this.usageRecordRepository.insertUsageRecord(12L, "1AAA", "ZA-BANK",
                String.valueOf(UsageRecord.MessageStatus.Completed), "ZAR", BigDecimal.valueOf(11005.150), LocalDateTime.of(2019, 11, 15, 15, 16));
    }

    private void sendToSampleAPI() {
        String charset = "UTF-8";
        File uploadFile1 = new File(FILE_PATH);
        try {
            MultipartUtility multipart = new MultipartUtility(HTTP_LOCALHOST_9091_API_UPLOAD, charset);

            multipart.addHeaderField("User-Agent", "CodeJava");
            multipart.addHeaderField("Test-Header", "Header-Value");

            multipart.addFormField("description", "Extracted File");
            multipart.addFormField("keywords", "Java,upload,Spring");

            multipart.addFilePart("file", uploadFile1);

            List<String> response = multipart.finish();

            System.out.println("SERVER REPLIED:");
            response.stream().forEach(System.out::println);

        } catch (IOException ex) {
            System.err.println(ex);
            throw new RuntimeException(ex.getMessage());
        }
    }
}
