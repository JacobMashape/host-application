package billingintegration.demo.scenario2;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Scenario2 {

    public static void main(String[] args) {
        List<String> statuses = Arrays.asList("Received", "Pending", "", "Awaiting Maturity", "Completed", "");
        List result = statuses.stream().filter(s -> !s.isEmpty()).collect(Collectors.toList());
        System.out.println("-------\nRunning sequential\n-------");
        result.stream().forEach(System.out::println);
        System.out.println("-------\nRunning parallel\n-------");
        result.parallelStream().forEach(System.out::println);
    }
}
