#Database Credentials
All database related info is placed in application.properties file.
#Host Application  
-Database name = billinghost

-password=absa

-user=Absa

#Assumtions Made
The date format for the table structure since datetime was specied as the actual date format is YYYY-MM-DD HH:MM:SS same applies for the history DB kept for the Billing File Layout. But the actual file will output the requred format (i.e. YYYYMMDD).

The file.txt file is created under *src\main\resources\extractedRecords\\* folder and sent to the Sample API application and saved under the same folder structure.

No justification alignment was explictly mentioned thus the right align justification is used in attempt to make the Billing File more readerble. 

#To run via cmd or powershell or bash
-mvn spring-boot:run


#Unit Tests 
I was very uncertain on how to go about it given the method that spawns the main algorithm returns nothing. I know i can make it return but not sure what exactly to return. Moreover the the method makes multiple calls to the DB and finally to the Sample API.

The only thing I knew I must test was the cronolical expression but time and lack of understanding hindered my progress on that front.  
